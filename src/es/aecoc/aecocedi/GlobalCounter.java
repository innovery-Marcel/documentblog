package es.aecoc.aecocedi;

public class GlobalCounter {

	static int c;
	static double averageTime;
	static double maxTime;
	static double minTime;
	static boolean first;
	
	public GlobalCounter(int i){
		c = i;
		averageTime = 0.0;
		maxTime = 0.0;
		minTime = 0.0;
		first = true;
	}
	
	public void addDoc(double dur){		
		double tmp = dur/1000000000;
		averageTime = ((averageTime * c) + (tmp)) / (c+1);
		c++;
		if (first){
			first = false;
			minTime = tmp;
		} else {
			if (tmp < minTime){
				minTime = tmp;
			}
		}
		if (tmp > maxTime){
			maxTime = tmp;
		}
	}
	
	public int getNumDocs(){
		return c;
	}
	
	public double getAverageTime(){
		return averageTime;
	}
	
	public double getMinTime(){
		return minTime;
	}
	
	public double getMaxTime(){
		return maxTime;
	}
	
	public void reset(){
		c = 0;
		averageTime = 0.0;
		maxTime = 0.0;
		minTime = 0.0;
		first = true;
	}
	

}