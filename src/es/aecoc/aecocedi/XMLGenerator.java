package es.aecoc.aecocedi;

import java.io.IOException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import org.apache.commons.dbutils.DbUtils;

public class XMLGenerator {
	/*Este c�digo lo ha subido AG el d�a 05/06/2018*/
	public static String generateXML(String fContent, GlobalConfiguration cc) throws AddressException, MessagingException, IOException {
		String fContentProc;
		String partyType;
		String gln;
		String tpB;
		String tpE;
		Integer indB;
		Integer indE;
		ResultSet rs = null;
		ResultSet rs2 = null;
		Connection conn;
		StringBuilder sb;
		try {
			/*AG: La URL de la cadena de conexi�n se pasa fija a la funci�n getConnection por un problema durante la actualizaci�n del sistema operativo*/
			/*conn = DriverManager.getConnection(cc.url,cc.dbUser,cc.dbPassword);*/
			conn = DriverManager.getConnection("jdbc:sqlserver://aecocedisql:1433;databasename=WEB_AECOC_PRO;integratedSecurity=false",cc.dbUser,cc.dbPassword);
		} catch (SQLException e) {
			ExceptionHandler.notifyAndPrintError(e, cc.emailcfg);
			return null;
		}
		Statement st;
		try {
			st = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
		} catch (SQLException e) {
			ExceptionHandler.notifyAndPrintError(e, cc.emailcfg);
			return null;
		}
		for (int i = 0; i < cc.partyTypes.size(); i++) {
			fContentProc = fContent;
			partyType = cc.partyTypes.get(i);
			sb = new StringBuilder();
			sb.append("<");
			sb.append(partyType);
			tpB = sb.toString();
			sb = new StringBuilder();
			sb.append("</");
			sb.append(partyType);
			tpE = sb.toString();
			indB = fContentProc.indexOf(tpB);
			indE = fContentProc.indexOf(tpE);
			if((partyType.equals("receiver") && fContent.contains("receiverCorporateOffice")) || (partyType.equals("sender") && fContent.contains("senderCorporateOffice")) || partyType.equals("receiverCorporateOffice") || partyType.equals("senderCorporateOffice")){
				indB = -1;
			}
			if (indB != -1) {
				gln = getGLN(fContentProc, partyType);
				if (gln.length() > 0) {
					sb = new StringBuilder();
					sb.append("USE WEB_AECOC_PRO SELECT Companies.Name, Companies_PhisicalCompany.FiscalCode, Companies.Address, Companies.City, Companies.PostalCode, Companies_PhisicalCompany.Telephone FROM Companies INNER JOIN Companies_PhisicalCompany ON Companies.Id = Companies_PhisicalCompany.Id WHERE Companies.Id = '");
					sb.append(gln);
					sb.append("'");				
					try {
						rs = st.executeQuery(sb.toString());
					} catch (SQLException e) {
						ExceptionHandler.notifyAndPrintError(e, cc.emailcfg);
						return null;
					}					
					try {
						if (rs.next() == true) {
							String[] info = new String[9];
							info[0] = gln;
							info[1] = rs.getString(1);
							info[2] = rs.getString(2);
							String subs = fContentProc.substring(indB, indE);
							if (subs.indexOf("<Department>") != -1) {
								info[3] = fContentProc.substring(
										fContentProc.indexOf("<Department>", indB) + 12,
										fContentProc.indexOf("</Department>", indB));
							} else {
								info[3] = "N/A";
							}
							info[4] = rs.getString(3);
							info[5] = rs.getString(4);
							info[6] = rs.getString(5);
							if (subs.indexOf("<Employee>") != -1) {
								info[7] = fContentProc.substring(
										fContentProc.indexOf("<Employee>", indB) + 10,
										fContentProc.indexOf("</Employee>", indB));
							} else {
								info[7] = "N/A";
							}
							info[8] = rs.getString(6);
							sb = new StringBuilder();
							sb.append(tpB);
							sb.append(">\n");
							String between = sb.toString();
							for (int j = 0; j < cc.tags.size(); j++) {
								String tag = cc.tags.get(j);
								String inf = info[j];
								if (cc.exceptions.contains(inf) == false && inf != null) {
									sb = new StringBuilder();
									sb.append(between);
									sb.append("<");
									sb.append(tag);
									sb.append(">");
									sb.append(inf);
									sb.append("</");
									sb.append(tag);
									sb.append(">\n");
									between = sb.toString();
								}
							}
							sb = new StringBuilder();
							sb.append(fContentProc.substring(0, indB));
							sb.append(between);
							sb.append(tpE);
							sb.append(">");
							sb.append(fContentProc.substring(indE + tpE.length() + 1,fContentProc.length()));						
							fContent = sb.toString();
						} else {
							sb = new StringBuilder();
							sb.append("USE WEB_AECOC_PRO SELECT Companies.Name, Companies.Address, Companies.City, Companies.PostalCode FROM Companies WHERE Companies.Id = '");
							sb.append(gln);
							sb.append("'");	
							rs2 = st.executeQuery(sb.toString());
							if (rs2.next() == true) {
								String[] info2 = new String[9];
								info2[0] = gln;
								info2[1] = rs2.getString(1);
								info2[2] = "N/A";
								String subs = fContentProc.substring(indB, indE);
								if (subs.indexOf("<Department>") != -1) {
									info2[3] = fContentProc.substring(
											fContentProc.indexOf("<Department>", indB) + 12,fContentProc.indexOf("</Department>", indB));
								} else {
									info2[3] = "N/A";
								}
								info2[4] = rs2.getString(2);
								info2[5] = rs2.getString(3);
								info2[6] = rs2.getString(4);
								if (subs.indexOf("<Employee>") != -1) {
									info2[7] = fContentProc.substring(
											fContentProc.indexOf("<Employee>", indB) + 10,fContentProc.indexOf("</Employee>", indB));
								} else {
									info2[7] = "N/A";
								}
								info2[8] = "N/A";
								sb = new StringBuilder();
								sb.append(tpB);
								sb.append(">\n");
								String between2 = sb.toString();
								for (int j = 0; j < cc.tags.size(); j++) {
									String tag = cc.tags.get(j);
									String inf = info2[j];
									if (cc.exceptions.contains(inf) == false && inf != null) {
										sb = new StringBuilder();
										sb.append(between2);
										sb.append("<");
										sb.append(tag);
										sb.append(">");
										sb.append(inf);
										sb.append("</");
										sb.append(tag);
										sb.append(">\n");
										between2 = sb.toString();
									}
								}
								sb = new StringBuilder();
								sb.append(fContentProc.substring(0, indB));
								sb.append(between2);
								sb.append(tpE);
								sb.append(">");
								sb.append(fContentProc.substring(indE + tpE.length() + 1,fContentProc.length()));						
								fContent = sb.toString();
							} else {
							}
						}
					} catch (SQLException e) {
						ExceptionHandler.notifyAndPrintError(e, cc.emailcfg);
						return null;
					}
				} 
			}
		}
		DbUtils.closeQuietly(rs);
		DbUtils.closeQuietly(rs2);
		DbUtils.closeQuietly(st);
		DbUtils.closeQuietly(conn);
		//Replace all &
		fContent = fContent.replaceAll("&","&amp;");
		return fContent;
	}
	
	public static String getGLN(String xml, String type) {
		String res = "";
		Integer ind = xml.indexOf("<gln>", xml.indexOf(type));
		if (ind != -1) {
			res = xml.substring(ind + 5, ind + 18);
		}
		return res;
	}
}