package es.aecoc.aecocedi;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class GlobalConfiguration {

	//Web and email
	String env;
	String rootFolder;
	String loadFolder;
	String logsFolder;
	String logFile;
	String emailcfg;
	SimpleDateFormat sdfLog;	
	String url;
	String dbUser;
	String dbPassword;	
	ArrayList<String> partyTypes;
	ArrayList<String> tags;
	ArrayList<String> exceptions;
	ArrayList<String> docTypes;
	boolean invalidPDF = false;
	
	//Web
	String copyFolder;
	
	//Email
	String dbQuery;
	String mailSubject;
    String SMTP_HOST_NAME;
    String SMTP_AUTH_USER;
    String SMTP_AUTH_PWD;
    long interval;
    
	public GlobalConfiguration(String type, String root) throws IOException{
		//Parameters that are static and never change
		StringBuilder sb;
		sdfLog = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss:S");
		rootFolder = root;
		env = type;
		if(type.equals("web")){			
	    	sb = new StringBuilder();
	    	sb.append(root);
	    	sb.append("Config/web_error.cfg");
	    	emailcfg = sb.toString();
			sb = new StringBuilder();
			sb.append(root);
			sb.append("Process/");
			loadFolder = sb.toString();
			sb = new StringBuilder();
			sb.append(root);
			sb.append("Logs/web/");
			logsFolder = sb.toString();
			logFile = sb.toString();
			sb = new StringBuilder();
			sb.append(root);
			sb.append("tmp_web/");
			copyFolder = sb.toString();
			loadConfig(type);
		}		
		if(type.equals("email")){
	    	sb = new StringBuilder();
	    	sb.append(root);
	    	sb.append("Config/email_error.cfg");
	    	emailcfg = sb.toString();
			sb = new StringBuilder();
			sb.append(root);
			sb.append("tmp_email/");
			loadFolder = sb.toString();
			sb = new StringBuilder();
			sb.append(root);
			sb.append("Logs/email/");
			logsFolder = sb.toString();
			logFile = sb.toString();
			loadConfig(type);
		}
	}
	
	public void reloadConfig() throws IOException{
		loadConfig(env);
	}
	
	public void setLogFolder(long time) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
		StringBuilder sb = new StringBuilder();
		sb.append(rootFolder);
		sb.append("Logs/");
		sb.append(env);
		sb.append("/");
		sb.append(sdf.format(time));
		logsFolder = sb.toString();
	}
		
	public void setLogFile(long time) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		StringBuilder sb = new StringBuilder();
		sb.append(logsFolder);
		sb.append("/");
		sb.append(sdf.format(time));
		sb.append(".err");
		logFile = sb.toString();
	}
	
	public void loadConfig(String t) throws IOException {
		String line;
	    Integer section = -1;
	    partyTypes = new ArrayList<String>();
	    tags = new ArrayList<String>();
	    exceptions = new ArrayList<String>();
	    docTypes = new ArrayList<String>();
	    BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(rootFolder+"Config/"+t+".cfg"), "UTF8"));
		while ((line = br.readLine()) != null) {
			if(line.startsWith("#") == true){
				switch(line.substring(1, line.length())){
				case "partyTypes":
					section = 0;
					break;
				case "tags":
					section = 1;
					break;
				case "exceptions":
					section = 2;
					break;
				case "docTypes":
					section = 3;
					break;
				case "DBurl":
					section = 4;
					break;
				case "DBuser":
					section = 5;
					break;
				case "DBpassword":
					section = 6;
					break;
				case "DBquery":
					section = 7;
					break;
				case "interval":
					section = 8;
					break;
				case "SMTP_HOST_NAME":
					section = 9;
					break;
				case "SMTP_AUTH_USER":
					section = 10;
					break;
				case "SMTP_AUTH_PWD":
					section = 11;
					break;
				case "mailSubject":
					section = 12;
					break;
				}				
			} else {
				if (line.startsWith("//") == false && line.length() > 0){
					switch(section){
					case 0:
						partyTypes.add(line);
						break;
					case 1:
						tags.add(line);
						break;
					case 2:
						exceptions.add(line);
						break;
					case 3:
						docTypes.add(line);
						break;
					case 4:
						url = line;
						break;
					case 5:
						dbUser = line;
						break;
					case 6:
						dbPassword = line;
						break;
					case 7:
						dbQuery = line;
						break;
					case 8:
						interval = Long.parseLong(line);
						break;
					case 9:
						SMTP_HOST_NAME = line;
						break;
					case 10:
						SMTP_AUTH_USER = line;
						break;
					case 11:
						SMTP_AUTH_PWD = line;
						break;
					case 12:
						mailSubject = line;
						break;
					}		
				}
			}
		}
		br.close();
		line = null;
		section = null;
		br = null;	
	}
}