package es.aecoc.aecocedi;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;
import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;
import org.xml.sax.SAXException;
import net.sf.saxon.TransformerFactoryImpl;

public class PDFConverter {
	   
	//Funci�n que genera un fichero PDF a partir de un string que contiene el XML
    public static String generatePDF(String xmlPathOrData, GlobalCounter n, GlobalConfiguration gc) throws AddressException, MessagingException, IOException {
		Long startTime = System.nanoTime();
		Long endTime;
		double duration;
		StringBuilder sb = new StringBuilder();
		String fContent = null;
		String fileName = null;
		String pdf = null;
		if(gc.env.equals("web")){
			while(true){
				try {
					Thread.sleep(100);
					fContent = new String(Files.readAllBytes(Paths.get(xmlPathOrData)));
					break;
				} catch (IOException | InterruptedException e) {
					//Do nothing
				}
				if (System.nanoTime() - startTime > 5000000000.0){
					IOException ex = new IOException("Couldn't get " + xmlPathOrData);	
					ExceptionHandler.notifyAndPrintError(ex, gc.emailcfg, fContent);
					return null;
				}
			}
			if (fContent.length() < 10){
				IOException ex = new IOException("fContent is empty! // " + xmlPathOrData);	
				ExceptionHandler.notifyAndPrintError(ex, gc.emailcfg);
				return null;
			}
			fileName = xmlPathOrData.substring(xmlPathOrData.lastIndexOf("/")+1, xmlPathOrData.lastIndexOf("."));
			sb = new StringBuilder();			
			sb.append(gc.loadFolder);
			sb.append(fileName);
			sb.append(".pdf");
			pdf = sb.toString();
		}
		if(gc.env.equals("email")){
			fContent = xmlPathOrData;
		}   	
		String docType = getDocType(fContent, gc);
		if(gc.env.equals("email")){
			sb = new StringBuilder();		
			sb.append(gc.loadFolder);
			sb.append(docType.toUpperCase());
			sb.append("_");
			sb.append(System.currentTimeMillis());
			sb.append(".pdf");
			pdf = sb.toString();
		}
		try{
			fContent = XMLGenerator.generateXML(fContent, gc);
		} catch (IOException | MessagingException e) {
			ExceptionHandler.notifyAndPrintError(e, gc.emailcfg);
			gc.invalidPDF = true;
		}
		sb = new StringBuilder();
		sb.append(gc.rootFolder);
		sb.append("XSLT/");
		sb.append(docType);
		sb.append(".xslt");
		String xslt = sb.toString();
		sb = new StringBuilder();
		sb.append(gc.rootFolder);
		sb.append("Config/fop.xconf");
		String xconf = sb.toString();
		if(gc.invalidPDF == false){
			convertToPDF(xslt, fContent, pdf, xconf, gc);
		}
		if (gc.env.equals("web")){		
			Path source = Paths.get(pdf);				
			Path target = Paths.get(gc.copyFolder + fileName + ".pdf");
			try {
				Files.copy(source, target, REPLACE_EXISTING);
			} catch (IOException e) {
				ExceptionHandler.notifyAndPrintError(e, gc.emailcfg);
			}
			try {
				Files.delete(source);
			} catch (IOException e) {
				ExceptionHandler.notifyAndPrintError(e, gc.emailcfg);
			}
		}
		endTime = System.nanoTime();
		duration = (endTime - startTime);
    	n.addDoc(duration);
    	return pdf;
    }
    
    //Funci�n que devuelve el tipo de documento a partir de un string que contiene el XML
    public static String getDocType (String fContent, GlobalConfiguration gc){
    	String res = null;
    	String dd;
    	StringBuilder sb = null;
    	for (int i = 0; i < gc.docTypes.size(); i++){
    		sb = new StringBuilder();
    		dd = gc.docTypes.get(i);
    		sb.append(dd);
    		sb.append("xml");
    		if (fContent.contains(sb.toString())){
    			res = dd;
    			break;
    		}
    	}
    	return res;
    }
	
    //Funci�n que convierte un string que contiene el XML a PDF
	public static void convertToPDF(String xslt, String fContent, String pdf, String xconf, GlobalConfiguration cc) throws AddressException, MessagingException, IOException {	
		File xsltFile = new File(xslt);
		Source xmlSource = new StreamSource(new StringReader(fContent));
		FopFactory fopFactory;
		try {
			fopFactory = FopFactory.newInstance(new File(xconf));
		} catch (SAXException | IOException e) {
			ExceptionHandler.notifyAndPrintError(e, cc.emailcfg);
			return;
		}
		FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
		foUserAgent.getEventBroadcaster().addEventListener(new SysOutEventListener(cc.emailcfg));
		OutputStream out;
		try {
			out = new java.io.FileOutputStream(pdf);
		} catch (FileNotFoundException e) {
			ExceptionHandler.notifyAndPrintError(e, cc.emailcfg);
			return;
		}
		Fop fop;
		try {
			fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);
		} catch (FOPException e) {
			ExceptionHandler.notifyAndPrintError(e, cc.emailcfg);
			return;
		}
		TransformerFactory factory = new TransformerFactoryImpl();	
		Transformer transformer;
		try {
			transformer = factory.newTransformer(new StreamSource(xsltFile));
		} catch (TransformerConfigurationException e) {
			ExceptionHandler.notifyAndPrintError(e, cc.emailcfg, fContent);
			return;
		}	
		Result res;
		try {
			res = new SAXResult(fop.getDefaultHandler());
		} catch (FOPException e) {
			ExceptionHandler.notifyAndPrintError(e, cc.emailcfg);
			return;
		}
		try {
			transformer.transform(xmlSource, res);
		} catch (TransformerException e) {
			ExceptionHandler.notifyAndPrintError(e, cc.emailcfg);
			return;
		}
		try {
			out.close();
		} catch (IOException e) {
			ExceptionHandler.notifyAndPrintError(e, cc.emailcfg);
			return;
		}
	}
}