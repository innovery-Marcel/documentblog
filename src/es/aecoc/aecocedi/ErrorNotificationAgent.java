package es.aecoc.aecocedi;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class ErrorNotificationAgent{
	
	static String emailcfg;
	static String SMTP_HOST_NAME;
    static String SMTP_AUTH_USER;
    static String SMTP_AUTH_PWD;
    static String mailTo;
    static String mailSubject;
    static Properties props;
    static Authenticator auth;
    
    public ErrorNotificationAgent(GlobalConfiguration gc) throws IOException{
    	emailcfg = gc.emailcfg;
    	loadErrorConfig();
    	props = new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", SMTP_HOST_NAME);
        props.put("mail.smtp.auth", "true");
        auth = new SMTPAuthenticator();
    }
    
    public static void init(String conf) throws IOException{
    	emailcfg = conf;
    	loadErrorConfig();
    	props = new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", SMTP_HOST_NAME);
        props.put("mail.smtp.auth", "true");
        auth = new SMTPAuthenticator();
    }
    
	public void reloadErrorConfig() throws IOException{
		loadErrorConfig();
	}
 
    public static void send(String cfg, String errorMessage) throws AddressException, MessagingException, IOException {  
    	init(cfg);
    	Session mailSession = Session.getInstance(props, auth);
        MimeMessage message = new MimeMessage(mailSession);
		message.setSubject(mailSubject);
		message.setFrom(new InternetAddress(SMTP_AUTH_USER));
		message.addRecipient(Message.RecipientType.TO,new InternetAddress(mailTo));
		BodyPart messageBodyPart1 = new MimeBodyPart();
		String htmlText = errorMessage;
		messageBodyPart1.setContent(htmlText, "text/html");
		Multipart multipart = new MimeMultipart();    
		multipart.addBodyPart(messageBodyPart1);
		message.setContent(multipart);
    	Transport transport = mailSession.getTransport();
    	transport.connect();
    	Transport.send(message);
        transport.close();
    }
       
    public static class SMTPAuthenticator extends javax.mail.Authenticator {
        public PasswordAuthentication getPasswordAuthentication() {
           String username = SMTP_AUTH_USER;
           String password = SMTP_AUTH_PWD;
           return new PasswordAuthentication(username, password);
        }
    }
    
	public static void loadErrorConfig() throws IOException {
	    String line;
	    Integer section = -1;
	    BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(emailcfg), "UTF8"));    
		while ((line = br.readLine()) != null) {
			if(line.startsWith("#") == true){
				switch(line.substring(1, line.length())){
				case "SMTP_HOST_NAME":
					section = 1;
					break;
				case "SMTP_AUTH_USER":
					section = 2;
					break;
				case "SMTP_AUTH_PWD":
					section = 3;
					break;
				case "mailSubject":
					section = 4;
					break;
				case "notificationEmail":
					section = 5;
					break;
				}
				
			} else {
				if (line.startsWith("//") == false && line.length() > 0){
					switch(section){
					case 1:
						SMTP_HOST_NAME = line;
						break;
					case 2:
						SMTP_AUTH_USER = line;
						break;
					case 3:
						SMTP_AUTH_PWD = line;
						break;
					case 4:
						mailSubject = line;
						break;
					case 5:
						mailTo = line;
						break;
					}		
				}
			}
		}
		br.close();
	}
}