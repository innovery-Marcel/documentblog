package es.aecoc.aecocedi;

import java.io.IOException;
import java.text.SimpleDateFormat;
import javax.mail.MessagingException;
import org.apache.fop.events.Event;
import org.apache.fop.events.EventFormatter;
import org.apache.fop.events.EventListener;
import org.apache.fop.events.model.EventSeverity;

public class SysOutEventListener implements EventListener {

	static String cfg;
	static SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss:S");
	
	public SysOutEventListener(String cfgFile){
		cfg = cfgFile;
	}
	
    public void processEvent(Event event) {
        String msg = EventFormatter.format(event);
        EventSeverity severity = event.getSeverity();
        if (severity == EventSeverity.INFO) {
            //System.out.println("[INFO] " + msg);
        } else if (severity == EventSeverity.WARN) {
        	//System.out.println("[WARN] " + msg);
        } else if (severity == EventSeverity.ERROR) {
            try {
				ErrorNotificationAgent.send(cfg,msg);
			} catch (MessagingException | IOException e) {
				System.err.println(sdf1.format(System.currentTimeMillis()));
				e.printStackTrace();
			}
        	System.err.println("[ERROR] " + msg);
        } else if (severity == EventSeverity.FATAL) {
        	try {
        		ErrorNotificationAgent.send(cfg,msg);
			} catch (MessagingException | IOException e) {
				System.err.println(sdf1.format(System.currentTimeMillis()));
				e.printStackTrace();
			}
        	System.err.println("[FATAL] " + msg);
        } else {
            assert false;
        }
    }
}