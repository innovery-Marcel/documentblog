package es.aecoc.aecocedi;

import java.io.IOException;
import java.text.SimpleDateFormat;
import javax.mail.MessagingException;

public class ExceptionHandler {

	static Exception e;
	static String emailcfg;
	static SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	public ExceptionHandler(Exception cv){
		e = cv;
	}
	
	public static void notifyAndPrintError(Exception ex, String cfg) throws MessagingException, IOException{
		if (ex.getMessage().contains("El proceso no tiene acceso al archivo porque est� siendo utilizado por otro proceso.") == false) {
			ErrorNotificationAgent.send(cfg,ex.getMessage());
			System.err.println(sdf.format(System.currentTimeMillis()));
			ex.printStackTrace();
			System.err.println("");
		}
	}
	
	public static void notifyAndPrintError(Exception ex, String cfg, String xmlContent) throws MessagingException, IOException{
		if (ex.getMessage().contains("El proceso no tiene acceso al archivo porque est� siendo utilizado por otro proceso.") == false) {
			ErrorNotificationAgent.send(cfg,ex.getMessage());
			ErrorNotificationAgent.send(cfg,xmlContent);
			System.err.println(sdf.format(System.currentTimeMillis()));
			ex.printStackTrace();
			System.err.println("");
		}
	}
}