package es.aecoc.aecocedi;

import java.io.IOException;
import javax.mail.MessagingException;

public class WebPDF {

	//Objeto que contiene la configuraci�n global del programa compartida entre clases
	static GlobalConfiguration globalConf;
	//Agente de notificaci�n de errores por correo
	static ErrorNotificationAgent eAgent;
	//Identificador para distinguir entre el programa "web" y "email" ya que comparten classes
	static String type = "web";
	//Objeto para vigilar/escuchar un directorio en b�squeda de nuevos ficheros
	static FolderWatcher fw;
	//Contador global para controlar tiempos y documentos procesados
	static GlobalCounter cc;
	
	public static void main(String[] args) throws IOException, MessagingException, InterruptedException {
		//Inicializar a 0 el contador global
		cc = new GlobalCounter(0);
		//Inicializar la configuraci�n global con el tipo y el root del programa
		globalConf = new GlobalConfiguration(type,"C:/PDF/");
		//Inicializar el agente de notificaci�n de errores por correo
		eAgent = new ErrorNotificationAgent(globalConf);
		//Inicializar el FolderWatcher
		fw = new FolderWatcher(globalConf);
		//Empezar a escuchar/vigilar la carpeta
    	fw.watchDirectoryPath(cc, globalConf, eAgent); 	
	}
}