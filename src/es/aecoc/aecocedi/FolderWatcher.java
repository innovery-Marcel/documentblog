package es.aecoc.aecocedi;

import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.text.SimpleDateFormat;
import javax.mail.MessagingException;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

public class FolderWatcher {

	static Path path;
	
	public FolderWatcher(GlobalConfiguration gc) throws FileNotFoundException{
		setErr(gc);
		path = new File(gc.loadFolder).toPath();
	}
	
	public static void setErr(GlobalConfiguration gc) throws FileNotFoundException{
		Long time = (long) (System.currentTimeMillis());
		File folder;
		File file;
		FileOutputStream fos;
		PrintStream ps;
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM");
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");		
		if(gc.logsFolder.equals(sdf1.format(time)) == false || gc.logFile.equals(sdf2.format(time)) == false) {
			gc.setLogFolder(time);			
			folder = new File(gc.logsFolder);
    		if (!folder.exists()) {
    			folder.mkdir();
    		}
    		gc.setLogFile(time);
			file = new File(gc.logFile);
    		fos = new FileOutputStream(file, true);
			ps = new PrintStream(fos);
			System.setErr(ps);
    	}
	}
	
    public void watchDirectoryPath(GlobalCounter globCount, GlobalConfiguration gc,ErrorNotificationAgent eAgent) throws MessagingException, IOException {
    	StringBuilder sb;
    	try {
    		Boolean isFolder = (Boolean) Files.getAttribute(path,"basic:isDirectory", NOFOLLOW_LINKS);
            if (!isFolder) {
    			sb = new StringBuilder();
    			sb.append("Path: ");
    			sb.append(path);
    			sb.append(" is not a folder"); 			
                throw new IllegalArgumentException(sb.toString());
                
            }
        } catch (IOException e) {
        	ExceptionHandler.notifyAndPrintError(e, gc.emailcfg);
        }
        FileSystem fs = path.getFileSystem();
        try (WatchService service = fs.newWatchService()) {
            path.register(service, ENTRY_CREATE); 
            WatchKey key = null;
            while (true) {           	
                key = service.take();
                Kind<?> kind = null;
                for (WatchEvent<?> watchEvent : key.pollEvents()) {
                    kind = watchEvent.kind();
                    if (OVERFLOW == kind) {
                        continue; // loop
                    } else if (ENTRY_CREATE == kind) {
                        @SuppressWarnings("unchecked")
						Path newPath = ((WatchEvent<Path>) watchEvent).context();
                        if (newPath.toString().endsWith("xml")){                      	
                        	gc.reloadConfig();
                        	setErr(gc);                     	
                        	eAgent.reloadErrorConfig();                  	
                			sb = new StringBuilder();
                			sb.append(path.toString().replace("\\", "/"));
                			sb.append("/");
                			sb.append(newPath);
                        	PDFConverter.generatePDF(sb.toString(), globCount, gc);
                        	System.gc();
                        }
                    }
                }
                if (!key.reset()) {
                    break;
                }
                //System.gc();
            }
        } catch (IOException e) {
        	ExceptionHandler.notifyAndPrintError(e, gc.emailcfg);
        } catch (InterruptedException e) {
        	//Don't send email nor print trace, STOP button from GUI causes this exception
        	//ExceptionHandler.notifyAndPrintError(e, gc.emailcfg);
        }
    } 
}