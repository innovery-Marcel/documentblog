package es.aecoc.aecocedi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.io.FileUtils;

public class EmailPDF{

	/*Este c�digo lo ha subido AG el d�a 05/06/2018*/
	static GlobalConfiguration globalConf;
    static Properties props;
    static Authenticator auth;
    static Connection conn;
    static SimpleDateFormat sdfDB = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    static String rootF = "C:/PDF_Email/";
    static String previousIDs = "";
    static GlobalCounter cc = new GlobalCounter(0);
	    
    public static void main(String[] args) throws AddressException, MessagingException, IOException { 	   		   	
    	globalConf = new GlobalConfiguration("email",rootF);
    	String folderName;
		String date;
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM");
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");
		FileOutputStream fos = null;
		PrintStream ps = null;
    	ResultSet rs = null;
    	Statement st = null;
    	String docu = " ";
		int rows = -1;
    	while(true) {   		
    		long time = (long) (System.currentTimeMillis());
    		globalConf.reloadConfig();
    		System.out.println(globalConf.url);
    		System.out.println(globalConf.dbPassword);
    		System.out.println(globalConf.interval);
    		System.out.println(globalConf.logFile);
    		rows = -1;

        	String time2 = sdfDB.format(time-globalConf.interval - 10000);
        	
        	String time3log = sdfDB.format(time);
    		folderName = sdf1.format(time);
    		date = sdf2.format(time);
    		File folder = new File(globalConf.rootFolder+"Logs/email/" + folderName + "/");
    		if (!folder.exists()) {
    			folder.mkdir();
    		}
    		File file = new File(globalConf.rootFolder+"Logs/email/" + folderName + "/" + date + ".err");
    		try {
				fos = new FileOutputStream(file, true);
			} catch (FileNotFoundException e1) {
				ExceptionHandler.notifyAndPrintError(e1, globalConf.emailcfg);
			}
    		ps = new PrintStream(fos);
			System.setErr(ps);
	        props = new Properties();
	        props.put("mail.transport.protocol", "smtp");
	        props.put("mail.smtp.host", globalConf.SMTP_HOST_NAME);
	        props.put("mail.smtp.auth", "true");
	        auth = new SMTPAuthenticator(); 	        
			try {
				/*AG: La URL de la cadena de conexi�n se pasa fija a la funci�n getConnection por un problema durante la actualizaci�n del sistema operativo*/
				/*conn = DriverManager.getConnection(globalConf.url,globalConf.dbUser,globalConf.dbPassword);*/
				conn = DriverManager.getConnection("jdbc:sqlserver://aecocedisql:1433;databasename=WEB_AECOC_PRO;integratedSecurity=false","aecoc_pdf","aecocedipdf");
			} catch (SQLException e) {
				ExceptionHandler.notifyAndPrintError(e, globalConf.emailcfg);
			}
			try {
				st = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
			} catch (SQLException e) {
				ExceptionHandler.notifyAndPrintError(e, globalConf.emailcfg);
			}
			try {
				
				/*OLD*/
				//rs = st.executeQuery(globalConf.dbQuery + "'" + time2 + "'");
				/*END OLD*/
				
				/*NEW*/
			 	if (previousIDs.length() > 1){
			 		rs = st.executeQuery(globalConf.dbQuery + "'" + time2 + "' AND Documents.Id NOT IN(" + previousIDs + ")");	
			 	} else {
			 		rs = st.executeQuery(globalConf.dbQuery + "'" + time2 + "'");	
			 	}
				previousIDs = "";
				/*END NEW*/
				
			} catch (SQLException e) {
				ExceptionHandler.notifyAndPrintError(e, globalConf.emailcfg);
			}
			try {
				while (rs.next())
				{				
					docu += rs.getString(1) + ", ";
				}
				rs.last();
			} catch (SQLException e) {
				ExceptionHandler.notifyAndPrintError(e, globalConf.emailcfg);
			}
			try {
				rows = rs.getRow();
				if (docu != " ")
				{
					System.err.println(time2 + " <--> " + time3log + " - Documents to be sent: " + rows + " Documents: " + docu);
					docu = " ";
				}
				System.err.println(time2 + " <--> " + time3log + " - Documents to be sent: " + rows);
						
			} catch (SQLException e) {
				ExceptionHandler.notifyAndPrintError(e, globalConf.emailcfg);
			}
			try {
				rs.first();
			} catch (SQLException e) {
				ExceptionHandler.notifyAndPrintError(e, globalConf.emailcfg);
			}
			try {
				rs.previous();
			} catch (SQLException e) {
				ExceptionHandler.notifyAndPrintError(e, globalConf.emailcfg);
			}			
        	if(rows > 0){
	        	try {
					sendEmail(rs,cc);
				} catch (MessagingException e) {
					ExceptionHandler.notifyAndPrintError(e, globalConf.emailcfg);
				}
        	}       	
			DbUtils.closeQuietly(rs);
		    DbUtils.closeQuietly(st);
		    DbUtils.closeQuietly(conn);
        	System.gc();
			try {
				Thread.sleep(globalConf.interval);
			} catch (InterruptedException e) {
	        	//Don't send email nor print trace, STOP button from GUI causes this exception
	        	//ExceptionHandler.notifyAndPrintError(e, globalConf.emailcfg);
				break;
			}
		}
    }
    
    public static String getDocName(String blob){
    	int ind = blob.indexOf("<uniqueCreatorIdentification>");
    	if (ind != -1){
    		return blob.substring(ind+29,blob.indexOf("<", ind+29));
    	} else {
    		return null;
    	}
    }
    
    public static void sendEmail(ResultSet rs, GlobalCounter cc) throws AddressException, MessagingException, IOException {
    	ArrayList<MimeMessage> messages = new ArrayList<MimeMessage>();
    	String text = "";
    	String pdf = "";
    	String pdfFileName = "";
    	String docType = "";
    	String blob = "";
    	String fn = "";
    	Session mailSession = Session.getDefaultInstance(props, auth);
    	try {
			while(rs.next()){
				globalConf.invalidPDF = false;
				
				/*OLD*/
				//blob = rs.getString(2);
				/*END OLD*/					
				
				/*NEW*/
				blob = rs.getString(3);
				/*END NEW*/
				
				fn = getDocName(blob);
				docType = PDFConverter.getDocType(blob,globalConf);
				pdf = PDFConverter.generatePDF(blob, cc, globalConf);
				
				/*NEW*/
				previousIDs = previousIDs + String.valueOf(rs.getInt(1)) + ",";
				/*END NEW*/
				
				if (globalConf.invalidPDF == false){
					MimeMessage message = new MimeMessage(mailSession);
					message.setSubject(globalConf.mailSubject);
					message.setFrom(new InternetAddress(globalConf.SMTP_AUTH_USER));
					//String str = "rfontdevila@aecoc.es,rfontdevila@gs1es.org,roger.fontdevila@gmail.com,asesoria_aecocdata@aecoc.es";
					
					/*OLD*/
					//String str = rs.getString(1);
					/*END OLD*/
					
					/*NEW*/
					String str = rs.getString(2);
					/*END NEW*/
					
					if (str.contains(",")){
						List<String> emailList = Arrays.asList(str.split(","));
						for (int i = 0; i < emailList.size(); i++){
							message.addRecipient(Message.RecipientType.TO,new InternetAddress(emailList.get(i)));
						}
					} else {
						message.addRecipient(Message.RecipientType.TO,new InternetAddress(str));
					}					
					
					BodyPart messageBodyPart1 = new MimeBodyPart();
					switch(docType){
						case "invoic":
							text = "una nueva factura";
							pdfFileName = "Factura_" + fn;
							break;
						case "orders":
							text = "un nuevo pedido";
							pdfFileName = "Pedido_" + fn;
							break;
						case "genral":
							text = "un nuevo texto libre"; 
							pdfFileName = "Texto_Libre_" + fn; 
							break;
						case "coacsu":
							text = "una nueva relaci�n de documentos"; 
							pdfFileName = "Relaci�n_de_documentos_" + fn;
							break;
						case "recadv":
							text = "una nueva confirmaci�n de recepci�n"; 
							pdfFileName = "Confirmaci�n_de_recepci�n_" + fn;
							break;
						case "remadv":
							text = "una nueva confirmaci�n de pago"; 
							pdfFileName = "Confirmaci�n_de_pago_" + fn;
							break;
					}
					String htmlText = "<p>Tiene "+text+" en AecocEDI. Pulse sobre el enlace para acceder al documento:<br><br><a href=\"https://www.aecocedi.es/Documents/All\">https://www.aecocedi.es</a><br><br></p>"+
					"<a href=\"https://www.aecocedi.es/Documents/All\"><img src=\"cid:image\"></a><br><img src=\"cid:image2\">"+
					"<p>Este es un email autom�tico de la aplicaci�n AecocEDI. Por favor, no responda a este correo porque no ser� atendido.<br>"+
					"Para m�s informaci�n, contacte con nuestro Servicio de Atenci�n al Socio: 93 254 34 40 / sas@aecoc.es</p>";
					messageBodyPart1.setContent(htmlText, "text/html");
					MimeBodyPart messageBodyPart2 = new MimeBodyPart();   
					DataSource source = new FileDataSource(pdf);    
					messageBodyPart2.setDataHandler(new DataHandler(source));    
					messageBodyPart2.setFileName(pdfFileName + ".pdf");
					
					/*TEST*/
					if(messageBodyPart2.getFileName().endsWith(".pdf") == false){
						System.err.println(blob);
						System.err.println(fn);
						System.err.println(messageBodyPart2.getFileName());
					}
					/*TEST*/
					
					BodyPart messageBodyPart4 = new MimeBodyPart();
					messageBodyPart4 = new MimeBodyPart();
					DataSource fds = new FileDataSource(globalConf.rootFolder + "Images/logo.png");
					messageBodyPart4.setDataHandler(new DataHandler(fds));
					messageBodyPart4.setHeader("Content-ID", "<image>"); 
					messageBodyPart4.setFileName("LogoAecocEDI.png");
					BodyPart messageBodyPart5 = new MimeBodyPart();
					messageBodyPart5 = new MimeBodyPart();
					DataSource fds2 = new FileDataSource(globalConf.rootFolder + "Images/bar.png");
					messageBodyPart5.setDataHandler(new DataHandler(fds2));
					messageBodyPart5.setHeader("Content-ID", "<image2>"); 
					messageBodyPart5.setFileName("bar.png"); 
					Multipart multipart = new MimeMultipart();    
					multipart.addBodyPart(messageBodyPart1);     
					multipart.addBodyPart(messageBodyPart2);
					multipart.addBodyPart(messageBodyPart4);
					multipart.addBodyPart(messageBodyPart5);
					message.setContent(multipart); 
					messages.add(message);
				}
			}
			
			/*NEW*/
			if (previousIDs.length() > 1){
				previousIDs = previousIDs.substring(0, previousIDs.length() - 1);
			}
			/*END NEW*/
			
		} catch (SQLException | MessagingException e) {
			ExceptionHandler.notifyAndPrintError(e, globalConf.emailcfg);
		}
    	Transport transport = null;
		try {
			transport = mailSession.getTransport();
		} catch (NoSuchProviderException e1) {
			ExceptionHandler.notifyAndPrintError(e1, globalConf.emailcfg);
		}
		try {
			transport.connect();
		} catch (MessagingException e1) {
			ExceptionHandler.notifyAndPrintError(e1, globalConf.emailcfg);
		}
    	for (int i = 0; i < messages.size(); i++){
    		try {
				messages.get(i).saveChanges();
			} catch (MessagingException e) {
				ExceptionHandler.notifyAndPrintError(e, globalConf.emailcfg);
			}
    		try {
				Transport.send(messages.get(i));
			} catch (MessagingException e) {
				ExceptionHandler.notifyAndPrintError(e, globalConf.emailcfg);
			}
    	}
		try {
			transport.close();
		} catch (MessagingException e1) {
			ExceptionHandler.notifyAndPrintError(e1, globalConf.emailcfg);
		}
		if (globalConf.invalidPDF == false){
	        try {
				FileUtils.cleanDirectory(new File(globalConf.loadFolder));
			} catch (IOException e) {
				ExceptionHandler.notifyAndPrintError(e, globalConf.emailcfg);
			}
		}
		System.gc();
    }
    
    //Clase que genera la autenticaci�n SMTP necesaria para conectarse al MailEnable
    public static class SMTPAuthenticator extends javax.mail.Authenticator {
        public PasswordAuthentication getPasswordAuthentication() {
           String username = globalConf.SMTP_AUTH_USER;
           String password = globalConf.SMTP_AUTH_PWD;
           return new PasswordAuthentication(username, password);
        }
    }
}