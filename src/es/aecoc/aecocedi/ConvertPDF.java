package es.aecoc.aecocedi;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import static java.nio.file.StandardCopyOption.*;

public class ConvertPDF {
	
	static SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss:S");
	static String processingFolder = "C:\\PDF\\Process\\";
	static String webFolder = "C:\\PDF\\tmp_web\\";
	static String jarLogsFolder = "C:\\PDF\\Logs\\jar\\";
	static String detailedLogsFolder = "C:\\PDF\\Logs\\jar_detail\\";
	static Path tmpXML;
	static Path tmpPDF;
	static int timeOut = 10000;
	
	public static void main(String[] args) {		
		String input = args[0];
		input = args[0];
		
		//Get filename
		String fileName = null;
		int index1 = input.lastIndexOf("/");
		int index2 = input.lastIndexOf("\\");
		if (index1 != -1) {
			fileName = input.substring(index1+1,input.lastIndexOf("."));
		} else {
			if (index2 != -1) {
				fileName = input.substring(index2+1,input.lastIndexOf("."));
			} else {
				//Wrong file input
				//This is not going to print anywhere
				System.err.println("------------------------" + sdf.format(System.currentTimeMillis()) + "------------------------");
				System.err.println("Wrong file input -> C:\\PDF\\tmp_web\\fileName.xml");
				System.err.println("");
				return;
			}
		}
		
		//Set web error log		
		File file = new File(jarLogsFolder + fileName + ".err");
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(file, true);
		} catch (FileNotFoundException e1) {
			//Cannot open log file
			//This is not going to print anywhere
			e1.printStackTrace();
			return;
		}
		PrintStream ps = new PrintStream(fos);	
		System.setErr(ps);
		//Validate XML
		if (validateXML(input) == true) {					
			//Copy XML to processing folder
			String XMLtoCopy = webFolder + fileName + ".xml";
			String targetXML = processingFolder + fileName + ".xml";
			Path source = Paths.get(XMLtoCopy);				
			tmpXML = Paths.get(targetXML);		
			try {
				Files.copy(source, tmpXML, REPLACE_EXISTING);
			} catch (IOException e) {
				//Cannot copy the XML file
				System.err.println("Error al generar PDF");
				//Set detailed log
				File file2 = new File(detailedLogsFolder + fileName + ".err");
				FileOutputStream fos2 = null;
				try {
					fos2 = new FileOutputStream(file2, true);
				} catch (FileNotFoundException e1) {
					//Cannot open log file
					e1.printStackTrace();
					ps.close();
					return;
				}
				PrintStream ps2 = new PrintStream(fos2);
				System.setErr(ps2);
				System.err.println("------------------------" + sdf.format(System.currentTimeMillis()) + "------------------------");
				System.err.println("XML copy error -> Couldn't copy " + source.toString() + " to " + tmpXML.toString());
				e.printStackTrace();
				System.err.println("");
				ps.close();
				return;
			}
			//Generated PDF
			String finalPDF = processingFolder + fileName + ".pdf";		
			File f2 = new File(finalPDF);
			tmpPDF = Paths.get(finalPDF);
			
			//Set time-out
			long initTime = System.currentTimeMillis();
			while(true){
				//Check if PDF has been copied back to C:/PDF/tmp_web/
				if(f2.exists() && !f2.isDirectory()) { 
				    //PDF created! Exit while
					break;
				}
				//If PDF is never copied back, exit program in 10 seconds
				if(System.currentTimeMillis() - initTime > timeOut){
					//PDF creation failed
					System.err.println("Error al generar PDF");
					//Set detailed log
					File file2 = new File(detailedLogsFolder + fileName + ".err");
					FileOutputStream fos2 = null;
					try {
						fos2 = new FileOutputStream(file2, true);
					} catch (FileNotFoundException e1) {
						//Cannot open log file
						e1.printStackTrace();
						ps.close();
						return;
					}
					PrintStream ps2 = new PrintStream(fos2);
					System.setErr(ps2);
					System.err.println("------------------------" + sdf.format(System.currentTimeMillis()) + "------------------------");
					System.err.println("PDF generation error -> Check C:/PDF/Logs/web/yyyy-MM/ddMMyyyy for more details");
					System.err.println("");
					//Clear files
					System.setErr(ps);
					cleanUp();
					ps.close();
					return;
				}
			}
			//Clear files and finish
			cleanUp();
			ps.close();
		}
	}
	
	public static void cleanUp(){
		File f1 = new File(tmpXML.toString());
		if (f1.exists()){
			try {
				Files.delete(tmpXML);
			} catch (IOException e) {
				//Cannot clean/delete files from C:/PDF/Process
				System.err.println("Error al generar PDF");
				//Set detailed log
				File file2 = new File(detailedLogsFolder + "cleanUp.err");
				FileOutputStream fos2 = null;
				try {
					fos2 = new FileOutputStream(file2, true);
				} catch (FileNotFoundException e1) {
					//Cannot open log file
					e1.printStackTrace();
					return;
				}
				PrintStream ps2 = new PrintStream(fos2);
				System.setErr(ps2);
				System.err.println("------------------------" + sdf.format(System.currentTimeMillis()) + "------------------------");
				System.err.println("C:/PDF/Process clean-up error -> Couldn't delete " + tmpXML.toString());
				e.printStackTrace();
				System.err.println("");
				ps2.close();
				return;
			} 
		}
	}
	
	public static boolean validateXML(String pathToFile) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(false);
		factory.setNamespaceAware(true);
		DocumentBuilder builder = null;
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			//Parsing error
			System.err.println("Error al generar PDF");
			//Set detailed log
			File file2 = new File(detailedLogsFolder + "cleanUp.err");
			FileOutputStream fos2 = null;
			try {
				fos2 = new FileOutputStream(file2, true);
			} catch (FileNotFoundException e1) {
				//Cannot open log file
				e1.printStackTrace();
				return false;
			}
			PrintStream ps2 = new PrintStream(fos2);
			System.setErr(ps2);
			System.err.println("------------------------" + sdf.format(System.currentTimeMillis()) + "------------------------");
			e.printStackTrace();
			System.err.println("");
			ps2.close();
			return false;
		}
		builder.setErrorHandler(new SimpleErrorHandler());    
		try {
			builder.parse(new InputSource(pathToFile));
		} catch (SAXException | IOException e) {
			//Parsing error
			System.err.println("Error al generar PDF");
			//Set detailed log
			File file2 = new File(detailedLogsFolder + "cleanUp.err");
			FileOutputStream fos2 = null;
			try {
				fos2 = new FileOutputStream(file2, true);
			} catch (FileNotFoundException e1) {
				//Cannot open log file
				e1.printStackTrace();
				return false;
			}
			PrintStream ps2 = new PrintStream(fos2);
			System.setErr(ps2);
			System.err.println("------------------------" + sdf.format(System.currentTimeMillis()) + "------------------------");
			e.printStackTrace();
			System.err.println("");
			ps2.close();
			return false;
		}
		return true;
	}
	
	public static class SimpleErrorHandler implements ErrorHandler {
	    public void warning(SAXParseException e) throws SAXException {
	        e.printStackTrace();
	    }

	    public void error(SAXParseException e) throws SAXException {
	    	e.printStackTrace();
	    }

	    public void fatalError(SAXParseException e) throws SAXException {
	    	e.printStackTrace();
	    }
	}
}